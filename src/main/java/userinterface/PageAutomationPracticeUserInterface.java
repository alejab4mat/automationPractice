package userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("http://automationpractice.com/index.php")
public class PageAutomationPracticeUserInterface extends PageObject {
    public static final Target SIGN_IN = Target.the("Sign in button").
            located(By.className("login"));
    public static final Target EMAIL_ADDRESS = Target.the("E-mail address field").
            located(By.id("email_create"));
    public static final Target CREATE_ACCOUNT = Target.the("Create an account button").
            located(By.id("SubmitCreate"));
    public static final Target FIRST_NAME = Target.the("First name field").
            located(By.id("customer_firstname"));
    public static final Target LAST_NAME = Target.the("Last name field").
            located(By.id("customer_lastname"));
    public static final Target PASSWORD = Target.the("Password field").
            located(By.id("passwd"));
    public static final Target ADDRESS = Target.the("Address field").
            located(By.id("address1"));
    public static final Target CITY = Target.the("City field").
            located(By.id("city"));
    public static final Target POSTAL_CODE = Target.the("Postal Code field").
            located(By.id("postcode"));
    public static final Target MOBILE_PHONE = Target.the("Mobile phone field").
            located(By.id("phone_mobile"));
    public static final Target LIST_OF_STATES = Target.the("List of States").
            located(By.id("id_state"));
    public static final Target REGISTER = Target.the("Register button").
            located(By.id("submitAccount"));
    public static final Target MESSAGE_TO_VALIDATE = Target.the("Tag Message to validate").
            located(By.className("page-heading"));
    public static final Target MESSAGE_CREATED_ACCOUNT = Target.the("Tag Message of created account").
            located(By.id("create_account_error"));
    public static final Target EMAIL = Target.the("E-mail field").
            located(By.id("email"));
    public static final Target PASSWORD_REGISTERED = Target.the("Password registered field").
            located(By.id("passwd"));
    public static final Target SIGN_IN_ON_PAGE = Target.the("Sign in button").
            located(By.id("SubmitLogin"));
}
