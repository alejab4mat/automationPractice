package interactions;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import userinterface.PageAutomationPracticeUserInterface;

import java.util.List;

public class SearchState implements Interaction {

    String state;

    public SearchState(String state) {
        this.state = state;
    }

    public static SearchState fromTheListAndChoise(String state) {
        return Tasks.instrumented(SearchState.class,state);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        for(int k=1; k<51; k++ ) {
            List<WebElement> listOfStates = PageAutomationPracticeUserInterface.LIST_OF_STATES.resolveFor(actor).findElements(By.xpath("//*[@value='"+k+"']"));
            for (int i = 0; i < listOfStates.size(); i++) {
                if ("Georgia".equals(listOfStates.get(i).getText())) {
                    listOfStates.get(i).click();
                    break;
                }
            }
        }
    }
}
