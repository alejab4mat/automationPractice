package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import userinterface.PageAutomationPracticeUserInterface;

import static userinterface.PageAutomationPracticeUserInterface.MESSAGE_CREATED_ACCOUNT;

public class TheMessage implements Question<String> {
    public static TheMessage ofCreatedAccount() {
        return new TheMessage();
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(MESSAGE_CREATED_ACCOUNT).viewedBy(actor).asString();
    }
}
