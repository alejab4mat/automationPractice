package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import userinterface.PageAutomationPracticeUserInterface;

import static userinterface.PageAutomationPracticeUserInterface.MESSAGE_TO_VALIDATE;

public class MessageOf implements Question<String> {
    public static MessageOf myAccount() {
        return new MessageOf();
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(MESSAGE_TO_VALIDATE).viewedBy(actor).asString();
    }
}
