package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Enter;
import userinterface.PageAutomationPracticeUserInterface;

import static userinterface.PageAutomationPracticeUserInterface.EMAIL_ADDRESS;

public class EnterThe implements Task {
    String email;

    public EnterThe(String email) {
        this.email = email;
    }

    public static EnterThe emailAddress(String email) {
        return Tasks.instrumented(EnterThe.class,email);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(email).into(EMAIL_ADDRESS));
    }
}
