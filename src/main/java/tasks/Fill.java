package tasks;

import interactions.SearchState;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Enter;
import userinterface.PageAutomationPracticeUserInterface;

import static userinterface.PageAutomationPracticeUserInterface.*;

public class Fill implements Task {
    String password;

    public Fill(String password) {
        this.password = password;
    }

    public static Fill theForm(String withPassword) {
        return Tasks.instrumented(Fill.class,withPassword);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue("Alejandra").into(FIRST_NAME),
                Enter.theValue("Blandon").into(LAST_NAME),
                Enter.theValue(password).into(PASSWORD),
                Enter.theValue("San Antonio").into(ADDRESS),
                Enter.theValue("Rionegro").into(CITY),
                Enter.theValue("00000").into(POSTAL_CODE),
                Enter.theValue("123456").into(MOBILE_PHONE),
                SearchState.fromTheListAndChoise("Georgia")
                );

    }
}
