package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import userinterface.PageAutomationPracticeUserInterface;

import static userinterface.PageAutomationPracticeUserInterface.CREATE_ACCOUNT;

public class ClickOnCreate implements Task {
    public static ClickOnCreate anAccounts() {
        return Tasks.instrumented(ClickOnCreate.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(CREATE_ACCOUNT));
    }
}
