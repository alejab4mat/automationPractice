package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static userinterface.PageAutomationPracticeUserInterface.SIGN_IN_ON_PAGE;

public class ClickOnSign implements Task {
    public static ClickOnSign in() {
        return Tasks.instrumented(ClickOnSign.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(SIGN_IN_ON_PAGE));
    }
}
