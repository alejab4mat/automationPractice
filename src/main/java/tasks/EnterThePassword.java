package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Enter;
import userinterface.PageAutomationPracticeUserInterface;

import static userinterface.PageAutomationPracticeUserInterface.PASSWORD_REGISTERED;

public class EnterThePassword implements Task {
    String inWebSite;

    public EnterThePassword(String inWebSite) {
        this.inWebSite = inWebSite;
    }

    public static EnterThePassword registered(String inWebSite) {
        return Tasks.instrumented(EnterThePassword.class,inWebSite);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(inWebSite).into(PASSWORD_REGISTERED));
    }
}
