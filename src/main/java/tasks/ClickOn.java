package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import userinterface.PageAutomationPracticeUserInterface;

import static userinterface.PageAutomationPracticeUserInterface.SIGN_IN;

public class ClickOn implements Task {
    public static ClickOn SignIn() {
        return Tasks.instrumented(ClickOn.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(SIGN_IN));
    }
}
