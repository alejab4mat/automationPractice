package stepsdefinition;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;
import questions.MessageOf;
import questions.TheMessage;
import tasks.*;
import userinterface.PageAutomationPracticeUserInterface;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class RegisterStepDefinition {

    @Managed(driver="chrome")
    private WebDriver navegadorChrome;

    @Before
    public void configuracionInicial() {
        OnStage.setTheStage(new OnlineCast());
        theActorCalled("user").can(BrowseTheWeb.with(navegadorChrome));
    }

    @Given("^The user is on Homepage$")
    public void theUserIsOnHomepage() {
        theActorInTheSpotlight().wasAbleTo(Open.browserOn(new PageAutomationPracticeUserInterface()));
    }

    @When("^the user clicks on Sign in$")
    public void theUserClicksOnSignIn() {
        theActorInTheSpotlight().attemptsTo(ClickOn.SignIn());
    }

    @When("^user enters email (.*)$")
    public void userEntersEmail(String required) {
        theActorInTheSpotlight().attemptsTo(EnterThe.emailAddress(required));
    }

    @When("^User Clicks create an account$")
    public void userClicksCreateAnAccount() {
        theActorInTheSpotlight().attemptsTo(ClickOnCreate.anAccounts());
    }

    @When("^User fills sign up form with password (.*)$")
    public void userFillsSignUpForm(String withPassword) {
        theActorInTheSpotlight().attemptsTo(Fill.theForm(withPassword));
    }

    @When("^user clicks register$")
    public void userClicksRegister() {
        theActorInTheSpotlight().attemptsTo(Clickregister.button());
    }

    @Then("^the user is taken my account page$")
    public void theUserIsTakenMyAccountPage() {
        theActorInTheSpotlight().should(seeThat(MessageOf.myAccount(), Matchers.equalTo("MY ACCOUNT")));
    }

    @Given("^the user has a valid account with email (.*)$")
    public void theUserHasAValidAccount(String required) {
        theActorInTheSpotlight().wasAbleTo(Open.browserOn(new PageAutomationPracticeUserInterface()),
                ClickOn.SignIn(),
                EnterThe.emailAddress(required),
                ClickOnCreate.anAccounts());
        theActorInTheSpotlight().should(seeThat(TheMessage.ofCreatedAccount(),
                Matchers.equalTo("An account using this email address has already been registered. Please enter a valid password or request a new one.")));
    }

    @When("^the clicks on sign in$")
    public void theClicksOnSignIn() {
        theActorInTheSpotlight().attemptsTo(ClickOn.SignIn());
    }

    @When("^enters email$")
    public void entersEmail() {
        theActorInTheSpotlight().attemptsTo(EnterTheEmail.address());
    }

    @When("^enters password (.*)$")
    public void entersPassword(String inWebSite) {
        theActorInTheSpotlight().attemptsTo(EnterThePassword.registered(inWebSite));
    }

    @When("^clicks sign in$")
    public void clicksSignIn() {
        theActorInTheSpotlight().attemptsTo(ClickOnSign.in());
    }

    @Then("^the user is successfully logged in$")
    public void theUserIsSuccessfullyLoggedIn() {
        theActorInTheSpotlight().should(seeThat(MessageOf.myAccount(), Matchers.equalTo("MY ACCOUNT")));
    }
}
